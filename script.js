
document.querySelector('.btn-play').addEventListener('click', (event) => new Popup());


class Player {
    #player;

    constructor(src) {
        this.onPlayerStateChange = this.onPlayerStateChange.bind(this);


        this.#player = new YT.Player('player', {
            'videoId': src.split('/embed/')[1],
            'events': {
                'onStateChange': this.onPlayerStateChange,
            }
        });

    }



    playPauseVideo() {
        console.log(this.#player);
        const state = this.#player.getPlayerState();
        if(state == YT.PlayerState.ENDED || state == YT.PlayerState.PAUSED) {
            this.#player.playVideo();
        } else {
            this.#player.pauseVideo();
        }
    }


    onPlayerStateChange(event) {
            document.querySelector('[data-focused]').focus();
    }
}


class Popup {
    #src;
    #closeBtn;
    #popup;
    #player;

    constructor(name) {
        this.openClosePopupHandler = this.openClosePopupHandler.bind(this);
        this.keydownHandler = this.keydownHandler.bind(this);

        this.#src = name || "https://www.youtube.com/embed/QbBWqdPGh3c";
        this.#popup = document.querySelector('.page__popup');
        this.#closeBtn = this.#createCloseBtn();
        this.#popup.appendChild(this.#closeBtn);
        this.#player = this.#getPlayer();

        this.#init();

    }

    //Методы жизненного цикла

    #init() {
        document.querySelector('iframe').classList.add('popup__content');
        document.querySelector('iframe').setAttribute('aria-label', 'play video zone');
        this.openClosePopupHandler();
        this.#closeBtn.addEventListener('click', this.openClosePopupHandler);
        document.addEventListener('keydown', this.keydownHandler);
    }


    #destroy() {
        this.#closeBtn.removeEventListener('click', this.openClosePopupHandler);
        document.removeEventListener('keydown', this.keydownHandler);
        this.#popup.innerHTML = '<div id="player"></div>';
    }


    //Обработчики событий


    openClosePopupHandler() {
        const isOpening = this.#popup.hidden;
        this.#popup.hidden = !isOpening;
        const page = document.querySelector('.page');
        if (isOpening) {
            page.classList.add('page-dark');
            page.setAttribute('aria-hidden', true);
            document.querySelector('.btn-play').setAttribute("tabindex", "-1");
            document.body.setAttribute("tabindex", "-1");
        } else {
            this.#destroy();
            page.classList.remove('page-dark');
            page.removeAttribute('aria-hidden');
            document.querySelector('.btn-play').focus();
            document.querySelector('.btn-play').removeAttribute("tabindex");
            document.body.removeAttribute("tabindex");
        }
    }


    keydownHandler(event) {

        if (event.code == 'Escape') {
            this.openClosePopupHandler();
        } else if (event.code == 'Tab' && !this.#popup.contains(document.activeElement)) {
            event.stopPropagation();
        } else if(event.code == 'Enter'  && document.activeElement !== this.#closeBtn.firstChild) {
            event.preventDefault();
        } else if(event.code == 'Space' && document.activeElement !== this.#closeBtn.firstChild ) {
            this.#player.playPauseVideo();
        }
    }




    //Вспомогательные функции


    #createElement(name) {
        return document.createElement(name);
    }


    #getPlayer() {
        return new Player(this.#src);
    }


    #createCloseBtn() {
        let closeBtnDiv = this.#createElement('div'),
            btnEl = this.#createElement('button'),
            hiddenInput = this.#createElement('input'),
            attributes = {
                "type": "button",
                "aria-label": "closing window button",
            };
        closeBtnDiv.classList.add('popup__close');
        btnEl.innerText = 'x';
        btnEl.classList.add('btn', 'btn-close');
        hiddenInput.setAttribute('data-focused', true);
        hiddenInput.setAttribute('tabindex', "-1");
        hiddenInput.style.opacity = 0;
        this.#setElementAttributes(btnEl, attributes);
        closeBtnDiv.appendChild(btnEl);
        closeBtnDiv.appendChild(hiddenInput);
        return closeBtnDiv;

    }

    #setElementAttributes(element, attributes) {
        for (let attribute in attributes) {
            element.setAttribute(attribute, attributes[attribute])
        }
    }
}

